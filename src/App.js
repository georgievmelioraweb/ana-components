import { Layout } from "@shopify/polaris";

import "@shopify/polaris/dist/styles.css";
import TopBar from "./components/TopBar";
import "./App.css";
function App() {
  return (
    <Layout>
      <Layout.Section>
        <TopBar />
      </Layout.Section>
    </Layout>
  );
}

export default App;
