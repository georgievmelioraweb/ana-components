import { Badge, Stack } from "@shopify/polaris";
import React from "react";

function BadgeComponent({ data }) {
  return (
    <Stack>
      {data.map((text) => (
        <Badge>{text}</Badge>
      ))}
    </Stack>
  );
}

export default BadgeComponent;
