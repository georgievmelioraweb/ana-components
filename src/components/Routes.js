import { Layout, Stack } from "@shopify/polaris";
import React from "react";
import EmptyStateCard from "./EmptyState";
import CheckboxList from "./Form Fields/CheckboxList";
import CustomActionList from "./Form Fields/CustomActionList";
import Datepicker from "./Form Fields/Datepicker";
import OptionsList from "./Form Fields/OptionsList";

// import MultiSelect from "./Form Fields/Multiselect";
// import SelectEx from "./Form Fields/Select";
import Map from "./Map";
import AddToRoute from "./Modals/AddToRoute";
import RoutesResourceList from "./RoutesResourceList";
import TotalCard from "./TotalCard";
function Routes() {
  const zoneOptions = [
    { label: "Uptown", value: "Uptown" },
    { label: "Midtown", value: "Midtown" },
    { label: "Downtown", value: "Downtown" },
  ];

  return (
    // <EmptyStateCard
    //   title="Orders"
    //   heading="There are no shipments for now"
    //   description="Try to refresh the page or come later"
    //   actionLabel="Refresh the page"
    //   img="./images/svgs/routes-illustration.svg"
    // />
    <>
      {/* <AddToRoute /> */}
      <Map />
      <div className="mt-4 mb-4">
        <Layout>
          <Layout.Section oneHalf>
            <OptionsList label="Choose date" content={<Datepicker />} />
          </Layout.Section>
          <Layout.Section oneHalf>
            <CustomActionList
              placeholder="Choose zones"
              // content={<CheckboxList options={zoneOptions} />}
              options={zoneOptions}
            />
          </Layout.Section>
        </Layout>
      </div>
      <TotalCard
        leftData={{ title: "Total Shipments", value: "98 out of 344" }}
      />
      <div className="mt-4 mb-4">
        <RoutesResourceList />
      </div>
    </>
  );
}

export default Routes;
