import React from "react";
import { Layout, Stack } from "@shopify/polaris";
import FilterField from "./Form Fields/FilterField";
import ZonesCard from "./ZonesCard";

import CustomActionList from "./Form Fields/CustomActionList";

function FilterComponent({ data, selected }) {
  const placeholder = selected === 0 ? "Search Zones" : "Search Slots";
  const label = selected === 0 ? "Zone name" : "Slot name";
  const zoneOptions = [
    { label: "Uptown", value: "Uptown" },
    { label: "Midtown", value: "Midtown" },
    { label: "Downtown", value: "Downtown" },
  ];

  const slotOptions = [
    { label: "Super fast delivery", value: "Super fast delivery" },
    { label: "Monday delivery", value: "Monday delivery" },
    { label: "Only Wednesday & Thursday", value: "Only Wednesday & Thursday" },
  ];

  const options = selected === 0 ? zoneOptions : slotOptions;
  return (
    <>
      <Layout>
        <Layout.Section>
          <FilterField placeholder={placeholder} />
        </Layout.Section>
        <Layout.Section secondary>
          <CustomActionList
            placeholder={label}
            label={label}
            options={options}
          />
        </Layout.Section>
      </Layout>

      <p className="fw-500 mt-5 mb-5">
        Showing {data.length} {selected === 0 ? "zones" : "slots"}
      </p>

      <ZonesCard items={data} selected={selected} />
    </>
  );
}

export default FilterComponent;
