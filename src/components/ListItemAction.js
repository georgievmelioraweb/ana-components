import React, { useCallback, useState } from "react";
import { Popover, ActionList, Button, Icon } from "@shopify/polaris";
import {
  EditMajor,
  DeleteMinor,
  MobileHorizontalDotsMajor,
} from "@shopify/polaris-icons";
function ListItemAction() {
  const [active, setActive] = useState(false);

  const toggleActive = useCallback(() => setActive((active) => !active), []);

  const handleImportedAction = useCallback(
    () => console.log("Imported action"),
    []
  );

  const handleExportedAction = useCallback(
    () => console.log("Exported action"),
    []
  );

  const activator = (
    <Button
      onClick={toggleActive}
      icon={<Icon source={MobileHorizontalDotsMajor} color="subdued" />}
      color="subdued"
      plain
    >
      {/* <Icon source={MobileHorizontalDotsMajor} color="subdued" /> */}
    </Button>
  );
  return (
    <div>
      <Popover active={active} activator={activator} onClose={toggleActive}>
        <ActionList
          items={[
            {
              content: "Edit slot",
              icon: EditMajor,
              onAction: handleImportedAction,
            },
            {
              content: "Delete Slot",
              icon: DeleteMinor,
              onAction: handleExportedAction,
              destructive: true,
            },
          ]}
        />
      </Popover>
    </div>
  );
}

export default ListItemAction;
