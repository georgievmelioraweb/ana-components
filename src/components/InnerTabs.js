import React, { useCallback, useState } from "react";
import { Card, Tabs } from "@shopify/polaris";

import InnerTabsContent from "./InnerTabsContent";

export default function InnerTabs({ tabs }) {
  const [selected, setSelected] = useState(0);

  const handleTabChange = useCallback(
    (selectedTabIndex) => setSelected(selectedTabIndex),
    []
  );

  return (
    <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange} fitted>
      <Card.Section>
        <InnerTabsContent selected={selected} />
      </Card.Section>
    </Tabs>
  );
}
