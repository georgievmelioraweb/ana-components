import React from "react";
import { Modal, TextContainer } from "@shopify/polaris";

function DeleteModal({ active, handleChange }) {
  return (
    <div>
      <Modal
        open={active}
        onClose={handleChange}
        title="Delete Zone"
        primaryAction={{
          content: "Delete",
          onAction: handleChange,
          id: "deleteBtn",
        }}
        secondaryActions={[
          {
            content: "Cancel",
            onAction: handleChange,
          },
        ]}
      >
        <Modal.Section>
          <div className="modal-content">
            <TextContainer>
              <p>Are you sure you want to delete the Midtown Zone?</p>
            </TextContainer>
          </div>
        </Modal.Section>
      </Modal>
    </div>
  );
}

export default DeleteModal;
