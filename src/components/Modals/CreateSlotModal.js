import React from "react";
import { Layout, Modal, Stack } from "@shopify/polaris";
import InputField from "../Form Fields/InputField";

import SelectWithLabel from "../Form Fields/SelectWithLabel";
import NumberField from "../Form Fields/NumberField";
import DiscountFields from "../Form Fields/DiscountFields";
function CreateSlotModal({ title, primaryAction, active, handleChange }) {
  const zoneOptions = [
    { label: "Uptown", value: "Uptown" },
    { label: "Midtown", value: "Midtown" },
    { label: "Downtown", value: "Downtown" },
  ];
  const dayOptions = [
    { label: "Monday", value: "Monday" },
    { label: "Tuesday", value: "Tuesday" },
    { label: "Wednesday", value: "Wednesday" },
    { label: "Thursday", value: "Thursday" },
    { label: "Friday", value: "Friday" },
    { label: "Saturday", value: "Saturday" },
    { label: "Sunday", value: "Sunday" },
  ];

  const blackouts = [
    { label: "Choose calendar to sync", value: "Choose calendar to sync" },
  ];
  return (
    <div>
      <Modal
        open={active}
        onClose={handleChange}
        title={title}
        primaryAction={{
          content: primaryAction,
          onAction: handleChange,
        }}
        secondaryActions={[
          {
            content: "Cancel",
            onAction: handleChange,
          },
        ]}
      >
        <Modal.Section>
          <div className="modal-content">
            <Layout>
              <Layout.Section>
                <Stack distribution="fillEvenly">
                  <InputField
                    label="Internal name"
                    value="Slot for Midtown_033"
                    name="internal_name"
                  />
                  <InputField
                    label="Name"
                    value="Super fast delivery"
                    name="internal_name"
                  />
                </Stack>
              </Layout.Section>
              <Layout.Section>
                <SelectWithLabel
                  placeholder="Choose zones to assign"
                  label="Assign to zones"
                  options={zoneOptions}
                />
              </Layout.Section>
              <Layout.Section>
                <SelectWithLabel
                  placeholder="Choose day to assign"
                  label="Choose day"
                  options={dayOptions}
                />
              </Layout.Section>
              <Layout.Section>
                <Stack distribution="fillEvenly">
                  <NumberField label="Starting at" />
                  <NumberField label="Ending at" />
                </Stack>
              </Layout.Section>
              <Layout.Section>
                <Stack distribution="fillEvenly">
                  <NumberField label="Max orders" />
                  <NumberField label="Base shipping price at" />
                </Stack>
              </Layout.Section>
              <Layout.Section>
                <DiscountFields />
              </Layout.Section>
              <Layout.Section>
                <SelectWithLabel
                  placeholder="Choose calendar to sync"
                  label="Blackouts"
                  options={blackouts}
                />
              </Layout.Section>
            </Layout>
          </div>
        </Modal.Section>
      </Modal>
    </div>
  );
}

export default CreateSlotModal;
