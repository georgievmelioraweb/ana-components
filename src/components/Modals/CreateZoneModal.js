import React from "react";
import { Modal, Layout, Stack } from "@shopify/polaris";
import InputField from "../Form Fields/InputField";
import CheckboxList from "../Form Fields/CheckboxList";
import Map from "../Map";

function CreateZoneModal({ active, handleChange }) {
  const slotsList = [
    { label: "Super fast delivery", value: "1" },
    { label: "Monday delivery", value: "2" },
    { label: "Only Wednesday & Thursday", value: "3" },
  ];
  return (
    <div>
      <Modal
        open={active}
        onClose={handleChange}
        title="Create Zone"
        primaryAction={{
          content: "Create",
          onAction: handleChange,
        }}
        secondaryActions={[
          {
            content: "Cancel",
            onAction: handleChange,
          },
        ]}
      >
        <Modal.Section>
          <Map />

          <div className="modal-content">
            <Layout>
              <Layout.Section>
                <InputField label="Zone Name" value="Midtown" name="zone" />
              </Layout.Section>
              <Layout.Section>
                <CheckboxList heading="Assigned Slots" options={slotsList} />
              </Layout.Section>
            </Layout>
          </div>
        </Modal.Section>
      </Modal>
    </div>
  );
}

export default CreateZoneModal;
