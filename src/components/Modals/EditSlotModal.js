import React from "react";
import { Layout, Modal, Stack } from "@shopify/polaris";
import InputField from "../Form Fields/InputField";
import CustomActionList from "../Form Fields/CustomActionList";

function EditSlotModal({ title, primaryAction, active, handleChange }) {
  const zoneOptions = [
    { label: "Uptown", value: "Uptown" },
    { label: "Midtown", value: "Midtown" },
    { label: "Downtown", value: "Downtown" },
  ];
  const dayOptions = [
    { label: "Monday", value: "Monday" },
    { label: "Tuesday", value: "Tuesday" },
    { label: "Wednesday", value: "Wednesday" },
    { label: "Thursday", value: "Thursday" },
    { label: "Friday", value: "Friday" },
    { label: "Saturday", value: "Saturday" },
    { label: "Sunday", value: "Sunday" },
  ];
  const starting = [{ label: "11:00", value: "11:00" }];
  const ending = [{ label: "12:00", value: "12:00" }];
  const maxOrders = [{ label: "30", value: "30" }];
  const shippingPrice = [{ label: "30", value: "30" }];
  const blackouts = [{ label: "Choose calendar to sync", value: "0" }];
  return (
    <div>
      <Modal
        open={active}
        onClose={handleChange}
        title={title}
        primaryAction={{
          content: primaryAction,
          onAction: handleChange,
        }}
        secondaryActions={[
          {
            content: "Cancel",
            onAction: handleChange,
          },
        ]}
      >
        <Modal.Section>
          <div className="modal-content">
            <Layout>
              <Layout.Section>
                <Stack distribution="fillEvenly">
                  <InputField
                    label="Internal name"
                    value="Slot for Midtown_033"
                    name="internal_name"
                  />
                  <InputField
                    label="Name"
                    value="Super fast delivery"
                    name="internal_name"
                  />
                </Stack>
              </Layout.Section>
              <Layout.Section>
                <CustomActionList
                  placeholder="Choose zones to assign"
                  label="Assign to zones"
                  options={zoneOptions}
                />
              </Layout.Section>
              <Layout.Section>
                <CustomActionList label="Choose day" options={dayOptions} />
              </Layout.Section>
              <Layout.Section>
                <Stack distribution="fillEvenly">
                  <CustomActionList label="Starting at" options={starting} />
                  <CustomActionList label="Ending at" options={ending} />
                </Stack>
              </Layout.Section>
              <Layout.Section>
                <Stack distribution="fillEvenly">
                  <CustomActionList label="Max orders" options={maxOrders} />
                  <CustomActionList
                    label="Base shipping price at"
                    options={shippingPrice}
                  />
                </Stack>
              </Layout.Section>
              <Layout.Section>
                <CustomActionList
                  placeholder="Choose calendar to sync"
                  label="Blackouts"
                  options={blackouts}
                />
              </Layout.Section>
            </Layout>
          </div>
        </Modal.Section>
      </Modal>
    </div>
  );
}

export default EditSlotModal;
