import React, { useCallback, useState } from "react";
import { Button, Stack, ChoiceList, Modal, Icon } from "@shopify/polaris";
import { PlusMinor } from "@shopify/polaris-icons";
export default function AddToRoute() {
  const CURRENT_PAGE = "current_page";
  const ALL_CUSTOMERS = "all_customers";
  const SELECTED_CUSTOMERS = "selected_customers";
  const CSV_EXCEL = "csv_excel";
  const CSV_PLAIN = "csv_plain";

  const [active, setActive] = useState(true);
  const [selectedExport, setSelectedExport] = useState([]);
  const [selectedExportAs, setSelectedExportAs] = useState([]);

  const handleModalChange = useCallback(() => setActive(!active), [active]);

  const handleClose = () => {
    handleModalChange();
    handleSelectedExport([]);
    handleSelectedExportAs([]);
  };

  const handleSelectedExport = useCallback(
    (value) => setSelectedExport(value),
    []
  );

  const handleSelectedExportAs = useCallback(
    (value) => setSelectedExportAs(value),
    []
  );

  const activator = <Button onClick={handleModalChange}>Open</Button>;

  return (
    <div>
      <Modal
        activator={activator}
        open={active}
        onClose={handleClose}
        title="Add to the route"
        primaryAction={{
          content: "Send directions",
          onAction: handleClose,
        }}
        secondaryActions={[
          {
            content: "Cancel",
            onAction: handleClose,
          },
        ]}
      >
        <Modal.Section>
          <div className="modal-content">
            <Stack vertical>
              <Stack.Item>
                <ChoiceList
                  title="Choose email"
                  choices={[
                    { label: "account1@gmail.com", value: 0 },
                    { label: "account2@gmail.com", value: ALL_CUSTOMERS },
                    { label: "account3@gmail.com", value: SELECTED_CUSTOMERS },
                  ]}
                  selected={selectedExport}
                  onChange={handleSelectedExport}
                />
              </Stack.Item>
              <Stack.Item>
                <Button plain fullWidth={true} textAlign="left">
                  <Icon source={PlusMinor} color="base" />
                  Add new email address
                </Button>
              </Stack.Item>
            </Stack>
          </div>
        </Modal.Section>
      </Modal>
    </div>
  );
}
