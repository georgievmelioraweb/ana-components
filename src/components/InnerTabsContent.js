import React, { useCallback, useState } from "react";
import {
  Card,
  Layout,
  Stack,
  Heading,
  DisplayText,
  Button,
} from "@shopify/polaris";
import FilterComponent from "./FilterComponent";
import CreateZoneModal from "./Modals/CreateZoneModal";
import EmptyState from "./EmptyState";
import CreateSlotModal from "./Modals/CreateSlotModal";
import EditSlotModal from "./Modals/EditSlotModal";
import TotalCard from "./TotalCard";
function InnerTabsContent({ selected }) {
  const [active, setActive] = useState(false);
  const handleChange = useCallback(() => setActive(!active), [active]);
  console.log(active);
  const zonesData = [
    {
      id: 100,
      url: "customers/341",
      name: "Midtown Zone",
      location: "#3122125",
      assigned: ["Monday Delivery", "Week day Delivery", "Fedex Delivery"],
      latestOrderUrl: "orders/1461",
    },
    {
      id: 100,
      url: "customers/341",
      name: "Midtown Zone",
      location: "#3122126",
      assigned: ["Monday Delivery"],
      latestOrderUrl: "orders/1460",
    },
    {
      id: 100,
      url: "customers/341",
      name: "Midtown Zone",
      location: "#3122126",
      assigned: [],
      latestOrderUrl: "orders/1459",
    },
  ];

  const slotsData = [
    {
      id: 100,
      url: "customers/341",
      name: "SUPER FAST DELIVERY",
      location: "#3122125",
      assigned: ["Downtown", "Midtown", "Uptown"],
      latestOrderUrl: "orders/1457",
    },

    {
      id: 100,
      url: "customers/341",
      name: "SUPER FAST DELIVERY",
      location: "#3122126",
      assigned: [],
      latestOrderUrl: "orders/1458",
    },
  ];

  const renderData = selected === 0 ? zonesData : slotsData;

  return (
    <div>
      <Layout>
        {renderData.length > 0 ? (
          <Layout.Section>
            <TotalCard
              leftData={
                selected === 0
                  ? { title: "Total Zones", value: "354" }
                  : { title: "Total Slots", value: "354" }
              }
              rightData={
                selected === 0
                  ? { title: "Zones without assigned slots", value: "54" }
                  : { title: "Slots not assigned", value: "54" }
              }
            />
          </Layout.Section>
        ) : null}
        <Layout.Section>
          <Card sectioned>
            <div className="d-flex justify-content-between align-items-center mb-4">
              <Heading size="medium">
                {selected === 0 ? "Zones" : "Slots"}
              </Heading>
              {renderData.length > 0 ? (
                <Button primary onClick={handleChange}>
                  Create {selected === 0 ? "zone" : "slot"}
                </Button>
              ) : null}
            </div>
            {renderData.length > 0 ? (
              <FilterComponent data={renderData} selected={selected} />
            ) : (
              <>
                {selected === 0 ? (
                  <EmptyState
                    heading="There are no zones for now"
                    description="You can create a new zone."
                    actionLabel="Create zone"
                    img="./images/svgs/map.svg"
                  />
                ) : (
                  <EmptyState
                    heading="There are no slots for now"
                    description="You can create a new slot."
                    actionLabel="Create slot"
                    img="./images/svgs/time.svg"
                  />
                )}
              </>
            )}
          </Card>
        </Layout.Section>
      </Layout>
      {selected === 0 ? (
        <CreateZoneModal
          title="Create Zone"
          primaryAction="Create"
          active={active}
          handleChange={handleChange}
        />
      ) : (
        <CreateSlotModal
          title="Create Slot"
          primaryAction="Create"
          active={active}
          handleChange={handleChange}
        />
      )}

      {/* <EditSlotModal
        title="Edit Slot"
        primaryAction="Save"
        active={active}
        handleChange={handleChange}
      /> */}
    </div>
  );
}

export default InnerTabsContent;
