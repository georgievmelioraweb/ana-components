import React from "react";
import { Card, Stack, DisplayText } from "@shopify/polaris";
function TotalCard({ selected, leftData, rightData }) {
  console.log("bool", rightData);
  return (
    <Stack distribution="fillEvenly">
      <Card sectioned>
        <div className="text-center">
          <p>{leftData.title}</p>
          <p className="bold-text mt-2">{leftData.value}</p>
        </div>
      </Card>
      {!rightData ? null : (
        <Card sectioned>
          <div className="text-center">
            <p>{rightData.title}</p>
            <p className="bold-text mt-2">{rightData.value}</p>
          </div>
        </Card>
      )}
    </Stack>
  );
}

export default TotalCard;
