import React from "react";
import { Card, EmptyState } from "@shopify/polaris";

function EmptyStateCard({ title, heading, actionLabel, description, img }) {
  return (
    <Card sectioned title={title}>
      <EmptyState
        heading={heading}
        action={{ content: actionLabel }}
        image={img}
      >
        <p>{description}</p>
      </EmptyState>
    </Card>
  );
}

export default EmptyStateCard;
