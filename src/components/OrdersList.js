import React, { useCallback, useState } from "react";
import {
  Avatar,
  TextStyle,
  Button,
  DatePicker,
  Card,
  ChoiceList,
  Filters,
  ResourceItem,
  ResourceList,
  Stack,
  Layout,
  Caption,
  TextContainer,
  Link,
  Collapsible,
  List,
  DisplayText,
  Thumbnail,
  Icon,
} from "@shopify/polaris";
import Datepicker from "./Form Fields/Datepicker";
import { LocationsMinor, PhoneMajor, EmailMajor } from "@shopify/polaris-icons";

export default function OrdersList() {
  const [selectedItems, setSelectedItems] = useState([]);
  const [orderStatus, setorderStatus] = useState(null);
  const [orderDate, setorderDate] = useState(null);
  const [zones, setZones] = useState(null);
  const [slots, setSlots] = useState(null);
  const [queryValue, setQueryValue] = useState(null);
  const [open, setOpen] = useState(false);

  const handleToggle = useCallback(() => setOpen((open) => !open), []);
  const handleorderStatusChange = useCallback(
    (value) => setorderStatus(value),
    []
  );
  const handleorderDateChange = useCallback((value) => setorderDate(value), []);
  const handleZonesChange = useCallback((value) => setZones(value), []);
  const handleSlotsChange = useCallback((value) => setSlots(value), []);
  const handleFiltersQueryChange = useCallback(
    (value) => setQueryValue(value),
    []
  );
  const handleorderStatusRemove = useCallback(() => setorderStatus(null), []);
  const handleorderDateRemove = useCallback(() => setorderDate(null), []);
  const handleZonesRemove = useCallback(() => setZones(null), []);
  const handleSlotsRemove = useCallback(() => setZones(null), []);
  const handleQueryValueRemove = useCallback(() => setQueryValue(null), []);
  const handleFiltersClearAll = useCallback(() => {
    handleorderStatusRemove();
    handleorderDateRemove();
    handleZonesRemove();
    handleSlotsRemove();
    handleQueryValueRemove();
  }, [
    handleorderStatusRemove,
    handleorderDateRemove,
    handleQueryValueRemove,
    handleZonesRemove,
    handleSlotsRemove,
  ]);

  const filters = [
    {
      key: "orderStatus",
      label: "Order status",
      filter: (
        <ChoiceList
          title="Order status"
          titleHidden
          choices={[
            { label: "All orders", value: "All orders " },
            { label: "Unfulfilled orders", value: "Unfulfilled orders " },
            { label: "Fulilled orders", value: "Fulilled orders " },
          ]}
          selected={orderStatus || []}
          onChange={handleorderStatusChange}
        />
      ),
      shortcut: true,
    },
    {
      key: "zones",
      label: "Zones",
      filter: (
        <ChoiceList
          title="Zones"
          titleHidden
          choices={[
            { label: "All orders", value: "All orders " },
            { label: "Unfulfilled orders", value: "Unfulfilled orders " },
            { label: "Fulilled orders", value: "Fulilled orders " },
          ]}
          selected={zones || []}
          onChange={handleZonesChange}
          allowMultiple
        />
      ),
      shortcut: true,
    },
    {
      key: "orderDate",
      label: "Order Date",
      filter: <Datepicker />,
      shortcut: true,
    },
    {
      key: "slots",
      label: "Slots",
      filter: (
        <ChoiceList
          title="Slots"
          titleHidden
          choices={[
            { label: "Super Fast Delivery", value: "Super Fast Delivery " },
            { label: "Fedex", value: "Fedex " },
            { label: "Fedex 2", value: "Fedex 2 " },
          ]}
          selected={slots || []}
          onChange={handleSlotsChange}
          allowMultiple
        />
      ),
      shortcut: true,
    },
  ];

  const appliedFilters = [];
  if (!isEmpty(orderStatus)) {
    const key = "orderStatus";
    appliedFilters.push({
      key,
      label: disambiguateLabel(key, orderStatus),
      onRemove: handleorderStatusRemove,
    });
  }
  if (!isEmpty(orderDate)) {
    const key = "orderDate";
    appliedFilters.push({
      key,
      label: disambiguateLabel(key, orderDate),
      onRemove: handleorderDateRemove,
    });
  }
  if (!isEmpty(zones)) {
    const key = "zones";
    appliedFilters.push({
      key,
      label: disambiguateLabel(key, zones),
      onRemove: handleZonesRemove,
    });
  }
  if (!isEmpty(slots)) {
    const key = "slots";
    appliedFilters.push({
      key,
      label: disambiguateLabel(key, slots),
      onRemove: handleSlotsRemove,
    });
  }
  const promotedBulkActions = [
    {
      content: "Actions",
      //   onAction: () => handleModalChange,
    },
    {
      content: "Cancel",
      onAction: () => console.log("Todo: implement bulk edit"),
    },
  ];

  return (
    <div>
      <Card>
        <ResourceList
          resourceName={{ singular: "customer", plural: "customers" }}
          filterControl={
            <Filters
              queryValue={queryValue}
              filters={filters}
              appliedFilters={appliedFilters}
              onQueryChange={handleFiltersQueryChange}
              onQueryClear={handleQueryValueRemove}
              onClearAll={handleFiltersClearAll}
            />
          }
          items={[
            {
              id: 112,
              url: "/",
              name: "Seong-Hyeon Kavalioŭ",
              location: "#3122125",
              latestOrderUrl: "/",
            },
          ]}
          renderItem={(item) => {
            const { id, name, location } = item;

            return (
              <>
                <ResourceItem
                  id={id}
                  accessibilityLabel={`View details for ${name}`}
                  persistActions
                >
                  <Card.Section>
                    <Layout>
                      <Layout.Section oneThird>
                        <h3 className="mb-2">
                          <TextStyle variation="strong">{name}</TextStyle>
                        </h3>
                        <div className="secondary-text">{location}</div>
                      </Layout.Section>
                      <Layout.Section oneThird>
                        <Stack distribution="equalSpacing" alignment="center">
                          <div>
                            <h3 className="mb-2">
                              <TextStyle variation="strong">
                                Downtown zone
                              </TextStyle>
                            </h3>
                            <Stack>
                              <p className="secondary-text">
                                Weekends delivery
                              </p>
                              <p className="secondary-text">
                                Sunday, 22/07/21, 14:00 - 16:00
                              </p>
                            </Stack>
                          </div>
                          <Stack distribution="trailing">
                            <img src="./images/svgs/assigned.svg" />
                            <Button plain onClick={handleToggle}>
                              <img src="./images/svgs/down-arrow.svg" />
                            </Button>
                          </Stack>
                        </Stack>
                      </Layout.Section>
                    </Layout>
                  </Card.Section>
                </ResourceItem>
                <Collapsible
                  open={open}
                  id="basic-collapsible"
                  transition={{
                    duration: "100ms",
                    timingFunction: "ease-in-out",
                  }}
                  expandOnPrint
                >
                  <Card.Section subdued>
                    <Stack distribution="fillEvenly">
                      <Card.Section title="Order Details">
                        <Stack vertical={true}>
                          <div className="row">
                            <div className="col-auto">
                              <Thumbnail
                                source="./images/svgs/Image.svg"
                                size="small"
                                alt="Black choker necklace"
                              />
                            </div>
                            <div className="d-flex flex-column col">
                              <p>Cat Food</p>
                              <Caption>x5.00</Caption>
                            </div>
                          </div>

                          <div className="row align-items-center">
                            <div className="col-auto">
                              <Thumbnail
                                source="./images/svgs/Image2.svg"
                                size="small"
                                alt="Black choker necklace"
                              />
                            </div>
                            <div className="d-flex flex-column col">
                              <p>
                                Navy Merino Wool Blazer with khaki chinosand
                                yellow belt
                              </p>
                              <Caption>x5.00</Caption>
                            </div>
                          </div>
                        </Stack>
                      </Card.Section>
                      <Card.Section title="Client Details">
                        <Stack vertical={true}>
                          <Stack>
                            <Icon
                              source={LocationsMinor}
                              color="base"
                              size="small"
                            />
                            <p class="small-caption">
                              Shlomo Ibn Gabirol St 100, Tel Aviv-Yafo
                            </p>
                          </Stack>
                          <Stack>
                            <Icon
                              source={PhoneMajor}
                              color="base"
                              size="small"
                            />
                            <p class="small-caption">+97235244040</p>
                          </Stack>
                          <Stack>
                            <Icon
                              source={EmailMajor}
                              color="base"
                              size="small"
                            />
                            <p class="small-caption">clientemail@gmail.com</p>
                          </Stack>
                        </Stack>
                      </Card.Section>
                    </Stack>
                  </Card.Section>
                </Collapsible>
              </>
            );
          }}
          selectedItems={selectedItems}
          onSelectionChange={setSelectedItems}
          selectable
          promotedBulkActions={promotedBulkActions}
        />
      </Card>
    </div>
  );

  function disambiguateLabel(key, value) {
    switch (key) {
      case "orderDate":
        return `Order Date is between $${value[0]} and $${value[1]}`;
      case "taggedWith":
        return `Tagged with ${value}`;
      case "orderStatus":
        return value.map((val) => `Customer ${val}`).join(", ");
      default:
        return value;
    }
  }

  function isEmpty(value) {
    if (Array.isArray(value)) {
      return value.length === 0;
    } else {
      return value === "" || value == null;
    }
  }
}
