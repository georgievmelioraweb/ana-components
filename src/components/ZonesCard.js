import React, { useCallback, useState } from "react";
import {
  TextStyle,
  Card,
  ResourceItem,
  ResourceList,
  Layout,
  Stack,
  InlineError,
  Button,
} from "@shopify/polaris";

import { MobileHorizontalDotsMajor } from "@shopify/polaris-icons";
import BadgeComponent from "./BadgeComponent";
import ListItemAction from "./ListItemAction";

export default function ZonesCard({ items, selected }) {
  const [selectedItems, setSelectedItems] = useState([]);
  return (
    <>
      <Card>
        <ResourceList
          resourceName={{ singular: "order", plural: "orders" }}
          items={items}
          selectedItems={selectedItems}
          onSelectionChange={setSelectedItems}
          selectable
          renderItem={(item) => {
            const { id, name, location, assigned, latestOrderUrl } = item;

            return (
              <ResourceItem
                id={id}
                accessibilityLabel={`View details for ${name}`}
              >
                <Stack vertical={false}>
                  <Stack.Item fill>
                    <Stack distribution="fillEvenly">
                      <Stack.Item>
                        <h3>
                          <TextStyle variation="strong">{name}</TextStyle>
                        </h3>
                        <p className="text-muted mt-2">{location}</p>
                      </Stack.Item>
                      <Stack.Item>
                        <h3 className="mb-3">
                          <TextStyle variation="strong">
                            {selected === 0
                              ? "Assigned slots"
                              : "Assigned to zones"}
                          </TextStyle>
                        </h3>
                        {assigned.length === 0 ? (
                          <Stack alignment="center">
                            <InlineError
                              message={
                                selected === 0
                                  ? "No slot assigned"
                                  : "No zones connected"
                              }
                              fieldID="myFieldID"
                            />
                            <Button>
                              {selected === 0 ? "Assign slot" : "Connect zone"}
                            </Button>
                          </Stack>
                        ) : (
                          <BadgeComponent data={assigned} />
                        )}
                      </Stack.Item>
                    </Stack>
                  </Stack.Item>
                  <Stack.Item>
                    <ListItemAction />
                  </Stack.Item>
                </Stack>
              </ResourceItem>
            );
          }}
        />
      </Card>
    </>
  );
}
