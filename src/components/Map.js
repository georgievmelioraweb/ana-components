import React from "react";
import {
  InfoWindow,
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  Polygon,
} from "react-google-maps";

import { DrawingManager } from "react-google-maps/lib/components/drawing/DrawingManager";

const polygonCords = [
  { lat: -34.397, lng: 150.644 },
  // { lat: 38.466, lng: -66.118 },
  // { lat: 32.321, lng: -64.757 },
  // { lat: 25.774, lng: -80.19 },
];
function Map() {
  const MapWithAMarker = withScriptjs(
    withGoogleMap((props) => (
      <GoogleMap
        defaultZoom={13}
        defaultCenter={{ lat: -34.397, lng: 150.644 }}
      >
        <Marker draggable={true} position={{ lat: -34.397, lng: 150.644 }}>
          <InfoWindow>
            <h2>Info goes here...</h2>
          </InfoWindow>
        </Marker>
        <Polygon
          path={polygonCords}
          key={1}
          editable={true}
          options={{
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.35,
          }}
        />

        <DrawingManager
          defaultDrawingMode={window.google.maps.drawing.OverlayType.POLYGON}
          defaultOptions={{
            drawingControl: true,
            drawingControlOptions: {
              position: window.google.maps.ControlPosition.TOP_CENTER,
              drawingModes: [window.google.maps.drawing.OverlayType.POLYGON],
            },
            polygonOptions: { editable: true },
          }}
        />
      </GoogleMap>
    ))
  );
  return (
    <>
      <MapWithAMarker
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=
AIzaSyBMwGejY12pZatM4UzXoLpNQdr5D_OKpIo&v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `360px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
      />
    </>
  );
}

export default Map;
