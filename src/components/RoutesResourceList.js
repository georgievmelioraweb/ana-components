import React, { useState, useCallback } from "react";
import {
  Avatar,
  TextStyle,
  Card,
  ResourceItem,
  ResourceList,
} from "@shopify/polaris";
import AddToRoute from "./Modals/AddToRoute";

export default function RoutesResourceList() {
  const [selectedItems, setSelectedItems] = useState([]);
  const [active, setActive] = useState(false);
  const handleModalChange = useCallback(() => setActive(!active), [active]);
  const resourceName = {
    singular: "order",
    plural: "orders",
  };

  const items = [
    {
      id: 111,
      url: "customers/231",
      name: "Seong Kavalioŭ \u2022 Tel-Aviv \u2022 Sunday, 22/07/21, 14:00 - 16:00",
      location: "#3122125, \u2022 623 Elm Street \u2022 Apartment 2 ",
      zone: "Midtown \u2022 Week day delivery",
      latestOrderUrl: "orders/1456",
    },
    {
      id: 211,
      name: "Seong Kavalioŭ \u2022 Tel-Aviv \u2022 Sunday, 22/07/21, 14:00 - 16:00",
      location: "#3122125, \u2022 623 Elm Street \u2022 Apartment 2 ",
      zone: "Midtown \u2022 Week day delivery",
      latestOrderUrl: "orders/1456",
    },
    {
      id: 311,
      name: "Seong Kavalioŭ \u2022 Tel-Aviv \u2022 Sunday, 22/07/21, 14:00 - 16:00",
      location: "#3122125, \u2022 623 Elm Street \u2022 Apartment 2 ",
      zone: "Midtown \u2022 Week day delivery",
      latestOrderUrl: "orders/1456",
    },
  ];

  const promotedBulkActions = [
    {
      content: "Add to the route",
      onAction: () => handleModalChange,
    },
    {
      content: "Cancel",
      onAction: () => console.log("Todo: implement bulk edit"),
    },
  ];

  //   const bulkActions = [
  //     {
  //       content: "Add tags",
  //       onAction: () => console.log("Todo: implement bulk add tags"),
  //     },
  //     {
  //       content: "Remove tags",
  //       onAction: () => console.log("Todo: implement bulk remove tags"),
  //     },
  //     {
  //       content: "Delete customers",
  //       onAction: () => console.log("Todo: implement bulk delete"),
  //     },
  //   ];

  return (
    <Card>
      {/* <AddToRoute /> */}
      <ResourceList
        resourceName={resourceName}
        items={items}
        renderItem={renderItem}
        selectedItems={selectedItems}
        onSelectionChange={setSelectedItems}
        promotedBulkActions={promotedBulkActions}
        // bulkActions={bulkActions}
        resolveItemId={resolveItemIds}
        // totalItemsCount={50}
        persistActions
      />
    </Card>
  );

  function renderItem(item, _, index) {
    const { id, url, name, location, zone, latestOrderUrl } = item;
    const media = <Avatar customer size="medium" name={name} />;
    const shortcutActions = latestOrderUrl
      ? [
          {
            content: "View latest order",
            accessibilityLabel: `View ${name}’s latest order`,
            url: latestOrderUrl,
          },
        ]
      : null;
    return (
      <ResourceItem
        id={id}
        // url={url}
        // media={media}
        sortOrder={index}
        accessibilityLabel={`View details for ${name}`}
      >
        <h3>
          <TextStyle variation="strong">{name}</TextStyle>
        </h3>
        <div className="mt-2 mb-2 secondary-text">{location}</div>
        <div className="secondary-text">{zone}</div>
      </ResourceItem>
    );
  }

  function resolveItemIds({ id }) {
    return id;
  }
}
