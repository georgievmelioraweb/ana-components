import React, { useCallback, useState } from "react";
import { Card, Tabs } from "@shopify/polaris";
import Map from "./Map";

import InnerTabs from "./InnerTabs";

import Dashboard from "./Dashboard";
import Routes from "./Routes";
export default function TabsComponent({ tabs }) {
  const [selected, setSelected] = useState(0);
  const zoneSlotData = [
    {
      id: "zones",
      content: "Zones",
      panelID: "zones",
    },
    {
      id: "slots",
      content: "Slots",
      panelID: "slots",
    },
  ];

  const handleTabChange = useCallback(
    (selectedTabIndex) => setSelected(selectedTabIndex),
    []
  );

  return (
    <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange} fitted>
      <Card.Section>
        {selected === 0 ? (
          <Dashboard />
        ) : selected === 1 ? (
          <>
            <Map />
            <InnerTabs tabs={zoneSlotData} />
          </>
        ) : selected === 2 ? (
          <Routes />
        ) : null}
      </Card.Section>
    </Tabs>
  );
}
