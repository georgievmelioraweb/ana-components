import React, { useCallback, useState } from "react";
import { Icon, TextField } from "@shopify/polaris";
import { SearchMinor } from "@shopify/polaris-icons";

function FilterField({ placeholder }) {
  const [textFieldValue, setTextFieldValue] = useState("");

  const handleTextFieldChange = useCallback(
    (value) => setTextFieldValue(value),
    []
  );
  return (
    <TextField
      type="text"
      placeholder={placeholder}
      value={textFieldValue}
      onChange={handleTextFieldChange}
      prefix={<Icon source={SearchMinor} color="base" />}
    />
  );
}

export default FilterField;
