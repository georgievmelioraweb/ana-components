import React, { useCallback, useState } from "react";
import { Popover, ActionList, Button } from "@shopify/polaris";
import RadioList from "./RadioList";
import Datepicker from "./Datepicker";

function OptionsList({ label, options, content }) {
  const [active, setActive] = useState(true);

  const toggleActive = useCallback(() => setActive((active) => !active), []);

  const handleImportedAction = useCallback(
    () => console.log("Imported action"),
    []
  );

  const handleClear = useCallback(() => console.log("Clear action"), []);

  const activator = (
    <Button fullWidth={true} textAlign="left" onClick={toggleActive} disclosure>
      {label}
    </Button>
  );
  return (
    <Popover
      fullWidth={true}
      active={active}
      activator={activator}
      onClose={toggleActive}
    >
      <ActionList
        items={[
          {
            content: <Datepicker />,
            onAction: handleImportedAction,
          },
          // {
          //   content: <RadioList options={options} />,
          //   onAction: handleImportedAction,
          // },
          // {
          //   content: <Button plain>Clear</Button>,
          //   onAction: handleClear,
          // },
        ]}
      />
    </Popover>
  );
}

export default OptionsList;
