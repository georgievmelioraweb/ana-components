import React, { useCallback, useState } from "react";
import { Stack, TextField, TextStyle } from "@shopify/polaris";

export default function NumberField({ label, disabled, type, symbol }) {
  const [value, setValue] = useState("0");

  const handleChange = useCallback((newValue) => setValue(newValue), []);

  return (
    <Stack alignment="center" distribution="fillEvenly">
      <TextField
        label={label}
        type="number"
        value={value}
        onChange={handleChange}
        disabled={disabled}
        fullWidth
      />
      {type === "discount" ? (
        <p className={disabled ? "text-subdued" : null}>{symbol}</p>
      ) : null}
    </Stack>
  );
}
