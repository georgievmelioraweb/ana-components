import React, { useCallback, useState } from "react";
import { ChoiceList } from "@shopify/polaris";

function RadioList({ options }) {
  const [selected, setSelected] = useState(["all-orders"]);

  const handleChange = useCallback((value) => setSelected(value), []);
  return (
    <ChoiceList choices={options} selected={selected} onChange={handleChange} />
  );
}

export default RadioList;
