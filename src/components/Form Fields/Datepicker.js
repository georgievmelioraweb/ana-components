import React, { useCallback, useState } from "react";
import { DatePicker } from "@shopify/polaris";

function Datepicker() {
  const [{ month, year }, setDate] = useState({ month: 8, year: 2021 });
  const [selectedDates, setSelectedDates] = useState({
    start: new Date("Thu Aug 07 2021 00:00:00 GMT-0500 (EST)"),
    end: new Date("Thu Aug 07 2021 00:00:00 GMT-0500 (EST)"),
  });

  const handleMonthChange = useCallback(
    (month, year) => setDate({ month, year }),
    []
  );

  return (
    <DatePicker
      month={month}
      year={year}
      onChange={setSelectedDates}
      onMonthChange={handleMonthChange}
      selected={selectedDates}
      fullWdth={true}
    />
  );
}

export default Datepicker;
