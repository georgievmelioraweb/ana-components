import React, { useCallback, useState } from "react";
import { Popover, ActionList, Button, ChoiceList } from "@shopify/polaris";

export default function CustomActionList({
  label,
  placeholder,
  options,
  initialValue,
}) {
  const [popoverActive, setPopoverActive] = useState(true);
  const [selected, setSelected] = useState([]);

  const b = selected;
  console.log(selected);
  console.log(
    "seleced",
    b.map((a) => a)
  );
  const togglePopoverActive = useCallback(
    () => setPopoverActive((popoverActive) => !popoverActive),
    []
  );

  const activator = (
    <>
      <Button
        onClick={togglePopoverActive}
        textAlign="left"
        fullWidth={true}
        disclosure="select"
      >
        {selected.length
          ? selected.map((el, id) =>
              id + 1 < selected.length ? el + ", " : el
            )
          : placeholder}
      </Button>
    </>
  );

  return (
    <>
      <Popover
        fullWidth={true}
        active={popoverActive}
        activator={activator}
        onClose={togglePopoverActive}
      >
        <ChoiceList
          title={placeholder}
          onChange={setSelected}
          choices={options}
          selected={selected}
          allowMultiple
        />
      </Popover>
    </>
  );
}
