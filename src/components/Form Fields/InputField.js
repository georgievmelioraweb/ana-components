import React, { useCallback, useState } from "react";
import { TextField } from "@shopify/polaris";

export default function InputField({ value, label, name }) {
  const [val, setVal] = useState(value);

  const handleChange = useCallback((newVal) => setVal(newVal), []);

  return (
    <TextField label={label} value={val} name={name} onChange={handleChange} />
  );
}
