import React, { useCallback, useState } from "react";
import { Stack, RadioButton } from "@shopify/polaris";
import NumberField from "./NumberField";

function DiscountFields() {
  const [value, setValue] = useState("fixed");

  return (
    <>
      <label className="mb-2">Discount price</label>
      <Stack distribution="fillEvenly">
        <Stack vertical>
          <RadioButton
            label="Fixed amount OFF"
            checked={value === "fixed"}
            id="fixed"
            name="fixed"
            onChange={() => setValue("fixed")}
          />
          <NumberField
            disabled={value !== "fixed" ? true : false}
            type="discount"
            symbol="$"
          />
        </Stack>
        <Stack vertical>
          <RadioButton
            label="Percentage"
            id="percentage"
            name="percentage"
            checked={value === "percentage"}
            onChange={() => setValue("percentage")}
          />
          <NumberField
            disabled={value !== "percentage" ? true : false}
            type="discount"
            symbol="% OFF"
          />
        </Stack>
      </Stack>
    </>
  );
}

export default DiscountFields;
