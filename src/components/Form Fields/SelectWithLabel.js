import React from "react";
import CustomActionList from "./CustomActionList";

function SelectWithLabel({ label, options, placeholder }) {
  return (
    <>
      <label className="Polaris-Label__Text mb-2">{label}</label>
      <CustomActionList placeholder={placeholder} options={options} />
    </>
  );
}

export default SelectWithLabel;
