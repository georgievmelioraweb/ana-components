import React, { useCallback, useState } from "react";
import { ChoiceList } from "@shopify/polaris";

function CheckboxList({ options }) {
  const [selected, setSelected] = useState(["hidden"]);

  const handleChange = useCallback((value) => setSelected(value), []);
  return (
    <ChoiceList
      allowMultiple
      // title="Zones"
      choices={options}
      selected={selected}
      onChange={handleChange}
    />
  );
}

export default CheckboxList;
