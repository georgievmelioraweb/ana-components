import React from "react";
import { DisplayText, ResourceList, Layout } from "@shopify/polaris";
import EmptyStateCard from "./EmptyState";
import TotalCard from "./TotalCard";

import ZonesCard from "./ZonesCard";
import Map from "./Map";
import OrdersList from "./OrdersList";
function Dashboard() {
  const ordersData = [
    {
      id: 100,
      url: "customers/341",
      name: "Seong-Hyeon Kavalioŭ",
      location: "#3122125",
      assigned: ["Monday Delivery", "Week day Delivery", "Fedex Delivery"],
    },
    {
      id: 100,
      url: "customers/341",
      name: "Seong-Hyeon Kavalioŭ",
      location: "#3122126",
      assigned: ["Monday Delivery"],
    },
    {
      id: 100,
      url: "customers/341",
      name: "Seong-Hyeon Kavalioŭ",
      location: "#3122126",
      assigned: [],
    },
  ];
  return (
    <>
      <Map />
      <div className="d-flex justify-content-center mt-3 mb-5">
        <p className="db-title">What's on my plate?</p>
      </div>
      {/*
      <EmptyStateCard
        title="Orders"
        heading="There are no orders for now"
        description="Try to refresh the page or come later"
        actionLabel="Refresh the page"
        img="./images/svgs/illustration.svg"
      /> */}

      <Layout>
        <Layout.Section>
          <TotalCard
            leftData={{ title: "Unfulfilled orders", value: "354" }}
            rightData={{ title: "Orders without slots", value: "85" }}
          />
        </Layout.Section>
        <Layout.Section>
          <OrdersList />
        </Layout.Section>
      </Layout>
    </>
  );
}

export default Dashboard;
