// import logo from "./logo.svg";
// import "./App.css";
// import { Page } from "@shopify/polaris";
// import "@shopify/polaris/dist/styles.css";
// import {
//   InfoWindow,
//   withScriptjs,
//   withGoogleMap,
//   GoogleMap,
//   Marker,
// } from "react-google-maps";

// import Geocode from "react-geocode";
// Geocode.setApiKey("AIzaSyAye_SWds9L0jSq6rHmaHyzvsMxH0Md-Uk");
// function App() {
//   const getCity = (addressArray) => {
//     let city = "";
//     addressArray.map((index, value) => {
//       if (
//         addressArray[index].types[0] &&
//         "administrative_area_level_2" === addressArray[index].types[0]
//       ) {
//         city = addressArray[index].long_name;
//       }
//     });
//   };
//   const handleDragEnd = (e) => {
//     let newLat = e.latLng.lat();
//     let newLng = e.latLng.lng();
//     console.log(newLat, newLng);
//     Geocode.fromLatLng(newLat, newLng).then((response) => {
//       console.log("response", response);
//       const address = response.results[0].formatted_address;
//       const addressArray = response.results[0].address_components;
//       const city = getCity(addressArray);
//       // const area = getArea(addressArray);
//       // const state = getState(addressArray);
//     });
//   };
//   const MapWithAMarker = withScriptjs(
//     withGoogleMap((props) => (
//       <GoogleMap
//         defaultZoom={10}
//         defaultCenter={{ lat: -34.397, lng: 150.644 }}
//       >
//         <Marker
//           draggable={true}
//           onDragEnd={handleDragEnd}
//           position={{ lat: -34.397, lng: 150.644 }}
//         >
//           <InfoWindow>
//             <h2>Info goes here...</h2>
//           </InfoWindow>
//         </Marker>
//       </GoogleMap>
//     ))
//   );
//   return (
//     <Page title="Polaris">
//       <MapWithAMarker
//         googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAye_SWds9L0jSq6rHmaHyzvsMxH0Md-Uk&v=3.exp&libraries=geometry,drawing,places"
//         loadingElement={<div style={{ height: `100%` }} />}
//         containerElement={<div style={{ height: `400px` }} />}
//         mapElement={<div style={{ height: `100%` }} />}
//       />
//     </Page>
//   );
// }

// export default App;
